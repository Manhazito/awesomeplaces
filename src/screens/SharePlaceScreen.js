import React, { Component } from "react"
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { connect } from "react-redux"

import PlacesList from "../components/PlacesList"

import { addPlace } from "../store/actions"

class SharePlaceScreen extends Component {
  placeSelectedHandler = (key) => {
    const selectedPlace = this.props.places.find(place => {
      return place.key === key
    })

    this.props.navigator.push({
      screen: "awesome.PlaceDetailsScreen",
      title: selectedPlace.name,
      passProps: {
        selectedPlace: selectedPlace
      }
    })
  }

  placeUnselectHandler = () => {
    // this.props.onDeselectPlace()
  }

  render() {
    return (
      <View style={styles.container}>
        <PlacesList places={this.props.places} onItemSelected={(key) => this.placeSelectedHandler(key)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 16,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

const mapStateToProps = state => {
  return {
    places: state.placesState.places
  }
}

export default connect(mapStateToProps, null)(SharePlaceScreen)