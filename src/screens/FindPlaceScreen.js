import React, { Component } from "react"
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { connect } from "react-redux"

import PlaceInput from "../components/PlaceInput"

import { addPlace } from "../store/actions"

class FindPlaceScreen extends Component {
  placeAddedHandler = (placeName) => {
    this.props.onAddPlace(placeName)
  }

  render() {
    return (
      <View style={styles.container}>
        <PlaceInput onPlaceAdded={this.placeAddedHandler} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 16,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

const mapDispatchToProps = dispatch => {
  return {
    onAddPlace: (name) => dispatch(addPlace(name)),
    onDeleteSelectedPlace: () => dispatch(deleteSelectedPlace())
  }
}

export default connect(null, mapDispatchToProps)(FindPlaceScreen)