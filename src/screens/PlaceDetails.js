import React, { Component } from "react"
import { View, Image, Text, Button, TouchableOpacity, StyleSheet } from "react-native"
import { connect } from "react-redux"

import { deleteSelectedPlace } from "../store/actions"

import Icon from "react-native-vector-icons/Ionicons"

class PlaceDetails extends Component {

  deleteSelectedPlaceHandler = () => {
    this.props.onDeleteSelectedPlace(this.props.selectedPlace.key)
    this.props.navigator.pop()
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={this.props.selectedPlace.image} style={styles.imageStyle} />
        <Text style={styles.textStyle}>{this.props.selectedPlace.name}</Text>
        <View>
          {/* <TouchableOpacity onPress={() => this.deleteSelectedPlaceHandler()}>
              <View style={{ alignItems: "center" }}>
                <Icon size={30} name={"ios-trash"} color="red" />
              </View>        
            </TouchableOpacity> */}
          <Button title={"Delete"} color="red" onPress={() => this.deleteSelectedPlaceHandler()} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //    marginTop: 16 // Needed on iOS
  },
  imageStyle: {
    width: "100%",
    height: 200,
    //    resizeMode: "contain"
  },
  textStyle: {
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 28
  }
})

const mapDispatchToProps = dispatch => {
  return {
    onDeleteSelectedPlace: (key) => dispatch(deleteSelectedPlace(key))
  }
}

export default connect(null, mapDispatchToProps)(PlaceDetails)