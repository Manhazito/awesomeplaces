Install Redux: "npm install redux react-redux"

Create the Store files: reducer(s) and actions files

Add Store configuration file.

Make the app aware of Redux: "index.js" file.

Connect Redux to the components: "App.js", for example.

