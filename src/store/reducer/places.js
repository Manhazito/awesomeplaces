import { ADD_PLACE, DELETE_SELECTED_PLACE } from "../actions";

const initialState = {
  places: []
}

export default placesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PLACE:
      return {
        ...state,
        places: state.places.concat({
          key: Math.random().toString(),
          name: action.placeName,
          image: { uri: "https://farm9.staticflickr.com/8554/29125586443_78178a9ad4_k.jpg" }
        })
      }
    case DELETE_SELECTED_PLACE:
      return {
        ...state,
        places: state.places.filter(place => {
          return place.key !== action.placeKey
        })
      }
    default:
      return state
  }
}