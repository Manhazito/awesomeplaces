export const ADD_PLACE = "ADD_PLACE"
export const DELETE_SELECTED_PLACE = "DELETE_SELECTED_PLACE"


export const addPlace = (placeName) => {
  return {
    type: ADD_PLACE,
    placeName: placeName
  }
}

export const deleteSelectedPlace = (key) => {
  return {
    type: DELETE_SELECTED_PLACE,
    placeKey: key
  }
}