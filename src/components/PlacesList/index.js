import React from "react"
import { FlatList, StyleSheet } from "react-native"

import ListItem from "../ListItem"

const placesList = props => {
  return (
    <FlatList
      style={styles.container}
      data={props.places}
      renderItem={(itemInfo) => (
        <ListItem
          placeName={itemInfo.item.name}
          placeImage={itemInfo.item.image}
          onItemPressed={() => props.onItemSelected(itemInfo.item.key)} />
      )} />
  )
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    paddingLeft: 16,
    paddingRight: 16
  }
})

export default placesList