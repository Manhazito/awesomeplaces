import React from "react"
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native"

const listItem = (props) => (
  <TouchableOpacity activeOpacity={0.5} onPress={props.onItemPressed}>
    <View style={styles.listItem}>
      <Image source={props.placeImage} style={{width: 45, height: 45}} />
      <Text style={{paddingLeft: 10, paddingRight: 10}}>{props.placeName}</Text>
    </View>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  listItem: {
    width: "100%",
//    padding: 2,
    marginBottom: 5,
    backgroundColor: "#eee",
    flexDirection: "row",
    alignItems: "center"
  }
})

export default listItem