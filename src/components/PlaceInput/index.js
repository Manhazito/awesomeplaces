import React, { Component } from "react"
import { View, TextInput, Button, StyleSheet } from "react-native"

class PlaceInput extends Component {
  state = {
    placeName: ""
  }

  placeNameChangedHandler = (value) => {
    this.setState({
      placeName: value
    })
  }

  placeSubmitHandler = () => {
    if (this.state.placeName.trim() === ""){
      return
    }
    // alert(this.state.placeName)

    this.props.onPlaceAdded(this.state.placeName)
    this.setState({
      placeName: ""
    })
  }

  render() {
    return (
      <View style={styles.inputContainer} >
        <TextInput
          style={styles.placeInput}
          placeholder="An awesome place…"
          value={this.state.placeName}
          onChangeText={this.placeNameChangedHandler} />
        <View style={styles.placeButtonContainer}>
          <Button
            onPress={this.placeSubmitHandler}
            title="Add" />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    width: "100%",
    paddingLeft: 16,
    paddingRight: 16,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: "space-between"
  },
  placeInput: {
    flex: 1,
//    width: "70%"
  },
  placeButtonContainer: {
    minWidth: 80,
//    width: "30%"
  },
});

export default PlaceInput