import { Navigation } from "react-native-navigation"
import { Provider } from "react-redux"
import configureStore from "./src/store/config"
import AuthScreen from "./src/screens/AuthScreen";
import SharePlaceScreen from "./src/screens/FindPlaceScreen";
import FindPlaceScreen from "./src/screens/SharePlaceScreen";
import PlaceDetails from "./src/screens/PlaceDetails";

const store = configureStore()

// Register Screens
Navigation.registerComponent("awesome.AuthScreen", () => AuthScreen, store, Provider)
Navigation.registerComponent("awesome.FindPlaceScreen", () => FindPlaceScreen, store, Provider)
Navigation.registerComponent("awesome.SharePlaceScreen", () => SharePlaceScreen, store, Provider)
Navigation.registerComponent("awesome.PlaceDetailsScreen", () => PlaceDetails, store, Provider)

// Start App
Navigation.startSingleScreenApp({
  screen: {
    screen: "awesome.AuthScreen",
    title: "Authentication"
  }
})